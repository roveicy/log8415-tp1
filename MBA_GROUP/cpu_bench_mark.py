#!/usr/bin/env python3
#mohammadreza rasolroveicy - thid code will run in a loop to do the benchmarking of cpu and network. you can change the number of prime number and threads respectiely 

import os
import sys
import time
import shutil
import cpuinfo
import platform
import socket
import subprocess
import multiprocessing
import errno
import datetime

num_tests = 5 #number of iterations  
file_first_run = True
temp_dir = "tmp"
filename = "./result/Benchmark.txt"

def size(size):

    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if size >= prefix[s]:
            value = float(size) / prefix[s]
            return '%.1f %sB' % (value, s)
    return "%sB" % size

def date(seconds):

	#Define the constants
	SECONDS_PER_MINUTE  = 60
	SECONDS_PER_HOUR    = 3600
	SECONDS_PER_DAY     = 86400

	#Calculate the days, hours, minutes and seconds
	days = seconds / SECONDS_PER_DAY
	seconds = seconds % SECONDS_PER_DAY

	hours = seconds / SECONDS_PER_HOUR
	seconds = seconds % SECONDS_PER_HOUR

	minutes = seconds / SECONDS_PER_MINUTE
	seconds = seconds % SECONDS_PER_MINUTE

	#Display the result
	return "%d Days, %02d Hours, %02d Minutes, %02d Seconds"%(days,hours,minutes,seconds)

# CPU Benchmark
def sysbenchCPU(prime=20000, threads=25):
    arg1 = "--cpu-max-prime=" + str(prime)
    with open(filename, "a") as output:
    	subprocess.call(["sysbench", "cpu", arg1, "--threads="+str(threads), "run"], stdout=output)

# CPU Benchmark - new run
print("\n-----------------------CPU Benchmark final run-------------------------\n")
for i in range(num_tests):
    sysbenchCPU(100000, 5000)
