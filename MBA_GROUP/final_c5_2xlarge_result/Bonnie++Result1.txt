Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency 100     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-87- 5632M   927  99 131449   9 131221   6  2486  99 5077694  99 12732 121
Latency              8668us   25763us   26556us    3585us      25us    2155us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-87-241    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                100   855   2 +++++ +++   572   1   853   2 +++++ +++   559   1
Latency               290ms     329us     141ms     272ms     130us     160ms
1.97,1.97,ip-172-31-87-241,100,1550859400,5632M,,927,99,131449,9,131221,6,2486,99,5077694,99,12732,121,100,,,,,855,2,+++++,+++,572,1,853,2,+++++,+++,559,1,8668us,25763us,26556us,3585us,25us,2155us,290ms,329us,141ms,272ms,130us,160ms
