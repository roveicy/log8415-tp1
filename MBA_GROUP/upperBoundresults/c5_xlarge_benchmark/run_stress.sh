#!/bin/bash
# Base on article https://la-vache-libre.org/stress-ng-un-outil-pratique-pour-tester-la-stabilite-des-composants-de-votre-machine-en-charge-elevee/

while getopts n:r: option
do
        case "${option}"
        in
                n) MEMORY_SIZE=$OPTARG;;
                r) RESULT=$OPTARG;;
        esac
done

#stress-ng --vm 1 $MEMORY_SIZE --timeout 10 --metrics-brief >> $RESULT.txt
stress-ng --cpu 1 --vm 1 --hdd 1 --fork 1 --switch 1 --timeout 10 --metrics >> result/memory.txt
