#!/bin/bash

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install build-essential -y
sudo apt install speedtest-cli -y

cmp=0
mkdir -p disk2 speed2
#______________________________________________________


for i in `seq 1 5`
do

echo "bensh \"$cmp\" is running "

#ToDo: IO benchmark using the upper bound of the lower machine
dd if=/dev/zero of=sb-io-test bs=7G count=100000 conv=fdatasync >> speed2/iotest"$cmp".txt 2>&1

#ToDo: Disk performance
#speed of a disk
sudo hdparm -Tt /dev/xvda >> disk2/disk"$cmp".txt 2>&1

sleep 20s

cmp=$((cmp+1))

done