#!/bin/bash
#Script for IOPS Bonnie++ and stress-ng bench marks for memory
#By: Biruk Asmare Muse
#Paramters are chosen to maimize load for each both bench marks

#First read the CPU and memory characterstics of the machine
#2>&1 |tee "CPUID.txt" #read details of the CPU
cpuid
free -g  #read the amount of memory in -m(mega) or -g(GB)

#Update the system
sudo apt update

#install stress-ng

sudo apt install stres-ng

#install bonnie++

sudo apt install bonnie++







#use the following command to run the script from your local machine
#<your connection string> 'bash -s'< Script.sh
#next download the generated files to your local machine using the following command
#scp -i <keyfile address>:/home/ubuntu/*.txt ./Bonnie++Result/


#stress-ng bench mark script
#install latest version using sudo snap install stress-ng
#===========================================
#bonnie++ upper bound for each instance
# ===========================================


## h12xlarge
sudo bonnie++ -c 200 -d /dev/tmp -u ubuntu -b -r 32G -s 15G -n 400 #upper bound


#r5large
sudo bonnie++ -c 200 -d /dev/tmp -u ubuntu -b -r 8G -s 7G -n 100

#c52xlarge
sudo bonnie++ -c 200 -d /dev/tmp -u ubuntu -b -r 8G -s 7G -n 100

#m4large
bonnie++ -c 200 -d /tmp -u ubuntu -b -r 8G -s 6G -n 100 #(lower bound)

#c5xlarge
sudo bonnie++ -c 200 -d /tmp -u ubuntu -b -r 8G -s 6G -n 200

#=======================================
#Bonnie++ Average value to run on all instances
#=======================================
# we can not average the memory size so we have to take the lower bound for -r and -s
sudo bonnie++ -c 200 -d /tmp -u ubuntu -b -r 8G -s 6G -n 180

#run the benchmark five times sequentially

for i in {1..5}
do
    sudo bonnie++ -c 200 -d /tmp -u ubuntu -b -r 8G -s 6G -n 180 2>&1 |tee "Bonnie++Result$i.txt"
done


#=========================================
#stress-ng Upper bound for each instance
#===========================================

#m4 large:
stress-ng --vm 2 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v is the upper bound

#c5.xlarge
stress-ng --vm 4 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v

#r5large
stress-ng --vm 4 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v

#h12xarge
stress-ng --vm 32 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v

#c52xlrge
stress-ng --vm 32 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v

#=================================================
#Average value of the parameters for stress-ng
#=================================================

stress-ng --vm 15 --vm-bytes 100% --vm-nethod all --verify --aggressive --meterics-brief -t 5m -v

#run stress-ng five times

for i in {1..5}
  do
    stress-ng --vm 15 --vm-bytes 100% --vm-nethod all --verify --aggressive --meterics-brief -t 5m -v 2>&1 |tee "Stress-ngResult$i.txt"

done






