#!/bin/bash
#benchmark.sh

#This script runs each benchmark sequentially and stores the result in result folder
#Each bench mark is run five times



#install benchmarks and their dependency
sudo apt-get update
sudo apt-get install sysbench
sudo apt-get install py-cpuinfo
sudo apt-get install cpuid
#install stress-ng
sudo snap install stress-ng

#install bonnie++

sudo apt install bonnie++
sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt-get install build-essential -y
sudo apt install speedtest-cli -y

#Gather information or characteristics of the machine and dumb it in a file

cpuid 2>&1 |tee "./result/CPUINFO.txt"
free -g |tee "./result/MemoryINFO.txt"
df |tee "./result/StorageINFO.txt" #To gather storage information

#run Disk benchmarks
echo "Starting hdparm...."
mkdir -p disk2 speed2
for i in {1..5}
    do
        sudo hdparm -Tt /dev/xvda1 2>&1 |tee "./result/HdParamResult$i.txt"
        echo "Finished hdparm run $i"
done

echo "Finished hdparm run"
#run IO and IOPS benchmark

#add dd here

echo "Starting dd benchmark..."
for i in {1..5}
    do

        dd if=/dev/zero of=sb-io-test bs=500M count=10 conv=fdatasync 2>&1 |tee "./result/DD$i.txt"
        sudo rm sb-io-test
        echo "Finished dd run $i"

    done

    echo "Finished dd run"

    echo "Starting Bonnie++..."

for i in {1..5}
do
  sudo bonnie++ -c 100 -d /tmp -u ubuntu -b -r 8G -s 5632M -n 100 2>&1 |tee "./result/Bonnie++Result$i.txt"
    echo "Finished bonnie++ run $i"
done

echo "Finished Bonnie++ benchmark"


#run network bench mark

echo "Starting network test..."

sudo python3 NetworkSpeedBenchmark.py

echo "Finished network test"


#run memory bench mark

echo "Starting stress-ng benchmark...."
for i in {1..5}
  do
    stress-ng --vm 15 --vm-bytes 100% --vm-method all --verify --aggressive --metrics-brief -t 5m -v 2>&1 |tee "./result/Stress-ngResult$i.txt"
    echo "Finished stress-ng run $i"
done

echo "Finished stress-ng benchmark..."

echo "Starting CPU benchmark"
#run CPU
sudo python3 cpu_bench_mark.py

echo "Finished CPU benchmark"

echo "============================="
echo "!!!!DONE!!!!"
echo "=============================="








